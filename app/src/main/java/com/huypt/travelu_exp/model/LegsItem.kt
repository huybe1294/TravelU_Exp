package com.huypt.travelu_exp.model

data class LegsItem(val duration: Duration,
                    val start_location: StartLocation,
                    val distance: Distance,
                    val start_address: String = "",
                    val end_location: EndLocation,
                    val end_address: String = "",
                    val steps: List<StepsItem>?)