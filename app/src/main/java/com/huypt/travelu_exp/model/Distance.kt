package com.huypt.travelu_exp.model

data class Distance(val text: String = "",
                    val value: Int = 0)