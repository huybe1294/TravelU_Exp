package com.huypt.travelu_exp.model

data class StepsItem(val duration: Duration,
                     val start_location: StartLocation,
                     val distance: Distance,
                     val travelMode: String = "",
                     val htmlInstructions: String = "",
                     val end_location: EndLocation,
                     val polyline: Polyline)