package com.huypt.travelu_exp.model

data class RoutesItem(val summary: String = "",
                      val copyrights: String = "",
                      val legs: List<LegsItem>?,
                      val bounds: Bounds,
                      val overviewPolyline: OverviewPolyline)