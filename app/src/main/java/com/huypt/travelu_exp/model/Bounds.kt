package com.huypt.travelu_exp.model

data class Bounds(val southwest: Southwest,
                  val northeast: Northeast)