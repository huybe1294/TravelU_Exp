package com.huypt.travelu_exp.model

data class GeocodedWaypointsItem(val types: List<String>?,
                                 val geocoderStatus: String = "",
                                 val placeId: String = "")