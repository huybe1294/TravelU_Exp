package com.huypt.travelu_exp.model

data class Directions(val routes: List<RoutesItem>?,
                      val geocodedWaypoints: List<GeocodedWaypointsItem>?,
                      val status: String = "")