package com.huypt.travelu_exp.model

data class Northeast(val lng: Double = 0.0,
                     val lat: Double = 0.0)