package com.huypt.travelu_exp.model

data class Duration(val text: String = "",
                    val value: Int = 0)