package com.huypt.travelu_exp

import android.Manifest
import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.hardware.display.DisplayManager
import android.hardware.display.VirtualDisplay
import android.media.projection.MediaProjection
import android.media.projection.MediaProjectionManager
import android.os.AsyncTask
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.PersistableBundle
import android.util.DisplayMetrics
import android.util.Log
import android.view.Surface
import android.view.SurfaceView
import android.view.animation.LinearInterpolator
import android.widget.Toast
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.maps.android.PolyUtil
import kotlinx.android.synthetic.main.activity_main.*
import pub.devrel.easypermissions.AfterPermissionGranted
import pub.devrel.easypermissions.EasyPermissions
import java.util.*

val SYDNEY = LatLng(21.026458, 105.799206)
val BAC_NINH = LatLng(21.178135, 106.070974)
val CAU_GIAY = LatLng(21.029974, 105.801595)
val ZOOM_LEVEL = 12f
const val REQUEST_CODE_LOCATION = 123

const val REQUEST_MEDIA_PROJECTION = 1
const val STATE_RESULT_CODE = "result_code"
const val STATE_RESULT_DATA = "result_data"

class MainActivity : AppCompatActivity(), OnMapReadyCallback {
    private lateinit var map: GoogleMap
    var myMarker: Marker? = null

    // Video record
    private var mScreenDensity: Int = 0
    private var mResultCode: Int = 0
    private var mResultData: Intent? = null

    private var mSurFace: Surface? = null
    private var mMediaProjection: MediaProjection? = null
    private var mVirtualDisplay: VirtualDisplay? = null
    private var mMediaProjectionManager: MediaProjectionManager? = null


    override fun onMapReady(googleMap: GoogleMap?) {
        map = googleMap ?: return
        enableMyLocation()
        with(googleMap) {
            moveCamera(CameraUpdateFactory.newLatLngZoom(SYDNEY, ZOOM_LEVEL))
            myMarker = addMarker(MarkerOptions().position(SYDNEY))
            setInfoWindowAdapter(InforWindowAdapter(this@MainActivity))
            myMarker?.showInfoWindow()
            addGroundOverlay(GroundOverlayOptions().image(
                    BitmapDescriptorFactory.fromResource(R.drawable.index))
                    .position(SYDNEY, 860f, 650f))  // latlng, width, height
        }
        DirectionTask().execute()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val mapFragment =
                supportFragmentManager.findFragmentById(R.id.map) as? SupportMapFragment
        mapFragment?.getMapAsync(this)

        if (savedInstanceState != null) {
            mResultCode = savedInstanceState.getInt(STATE_RESULT_CODE)
            mResultData = savedInstanceState.getParcelable(STATE_RESULT_DATA)
        }

        val metrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(metrics)
        mScreenDensity = metrics.densityDpi
        mMediaProjectionManager = getSystemService(Context.MEDIA_PROJECTION_SERVICE) as MediaProjectionManager
        mSurFace = surfaceView.holder.surface
        btnRecord.setOnClickListener {
            if (mVirtualDisplay == null) {
                startScreenCapture()
            } else {
                stopScreenCapture()
            }
        }
    }

    private fun stopScreenCapture() {
        if (mVirtualDisplay == null)
            return
        mVirtualDisplay?.release()
        mVirtualDisplay = null
        btnRecord.text = "Start"
    }

    private fun startScreenCapture() {
        if (mSurFace == null) {
            return
        }
        if (mMediaProjection != null) {
            setUpVirtualDisplay()
        } else if (mResultCode != 0 && mResultData != null) {
            setUpMediaProjection()
            setUpVirtualDisplay()
        } else {
            startActivityForResult(mMediaProjectionManager?.createScreenCaptureIntent(), REQUEST_MEDIA_PROJECTION)
        }
    }

    override fun onSaveInstanceState(outState: Bundle?, outPersistentState: PersistableBundle?) {
        super.onSaveInstanceState(outState, outPersistentState)
        if (mResultData != null) {
            outState?.putInt(STATE_RESULT_CODE, mResultCode)
            outState?.putParcelable(STATE_RESULT_DATA, mResultData)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_MEDIA_PROJECTION) {
            if (resultCode != Activity.RESULT_OK) {
                Log.i("MainActivity", "User cancelled")
                Toast.makeText(this, "User denied screen sharing permission", Toast.LENGTH_SHORT).show()
                return
            }
            mResultCode = resultCode
            mResultData = data
            setUpMediaProjection()
            setUpVirtualDisplay()
        }
    }

    private fun setUpMediaProjection() {
        mMediaProjection = mMediaProjectionManager?.getMediaProjection(mResultCode, mResultData)

    }

    private fun setUpVirtualDisplay() {
                mVirtualDisplay = mMediaProjection?.createVirtualDisplay(
                "ScreenCapture", surfaceView.width, surfaceView.height,
                        mScreenDensity, DisplayManager.VIRTUAL_DISPLAY_FLAG_AUTO_MIRROR, mSurFace,
                null, null)
        btnRecord.text = "Stop"
    }

    private fun tearDownMediaProjection() {
        if (mMediaProjection != null) {
            mMediaProjection?.stop()
            mMediaProjection = null
        }
    }

    public override fun onPause() {
        super.onPause()
        stopScreenCapture()
    }

    public override fun onDestroy() {
        super.onDestroy()
        tearDownMediaProjection()
    }


    @SuppressLint("MissingPermission")
    @AfterPermissionGranted(REQUEST_CODE_LOCATION)
    private fun enableMyLocation() {
        if (hasLocationPermission()) {
            map.isMyLocationEnabled = true
        } else {
            EasyPermissions.requestPermissions(this, getString(R.string.location),
                    REQUEST_CODE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION
            )
        }
    }

    private fun hasLocationPermission(): Boolean {
        return EasyPermissions.hasPermissions(this, Manifest.permission.ACCESS_FINE_LOCATION)
    }

    @SuppressLint("StaticFieldLeak")
    inner class DirectionTask : AsyncTask<Void, String, MutableList<String>>() {
        override fun doInBackground(vararg p0: Void?): MutableList<String> {
            val request = Constant.makeURL("21.026458", "105.799206",
                    "21.178135", "106.070974")

            return GetDirectionTask().getDirection(request)
        }

        override fun onPostExecute(result: MutableList<String>?) {
            super.onPostExecute(result)
            if (result != null) {
                val listLatLng = mutableListOf<LatLng>()

                val myPolyline: Polyline

                for (item in 0 until result.size) {
                    listLatLng.addAll(PolyUtil.decode(result[item]))
                }
                val lineOptions = PolylineOptions()
                with(lineOptions) {
                    width(10f)
                    color(Color.RED)
                    startCap(SquareCap())
                    endCap(SquareCap())
                }
                myPolyline = map.addPolyline(lineOptions)

                val listLatLngSize = listLatLng.size
                val duration: Long
                val zoomLevel: Float // City
                when (listLatLngSize) {
                    in 0..100 -> {
                        duration = 3000
                        zoomLevel = 15f
                    }

                    in 101..200 -> {
                        duration = 6000
                        zoomLevel = 14.5f
                    }

                    in 201..500 -> {
                        duration = 14000
                        zoomLevel = 14f
                    }

                    in 500..1000 -> {
                        duration = 20000
                        zoomLevel = 13.5f
                    }
                    else -> {
                        duration = 30000
                        zoomLevel = 13f
                    }
                }
                directionAnimate(duration, zoomLevel, listLatLng, myPolyline)
            }
        }

        private fun directionAnimate(duration: Long, zoomLevel: Float, listLatLng: MutableList<LatLng>, polyline: Polyline) {
            ValueAnimator.ofInt(0, listLatLng.size).apply {
                this.duration = duration
                interpolator = LinearInterpolator()
                addUpdateListener {
                    // onAnimationUpdate
                    val latlngList = polyline.points
                    val initPointSize = latlngList.size
                    val animateValue = it.animatedValue as Int

                    if (initPointSize < animateValue) {
                        map.moveCamera(CameraUpdateFactory.newLatLngZoom(listLatLng[initPointSize], zoomLevel))
                        myMarker?.position = listLatLng[initPointSize]
                        polyline.color = randomColor()
                        latlngList.addAll(listLatLng.subList(initPointSize, animateValue))
                        polyline.points = latlngList
                    }
                }
                start()
            }
        }

        private fun randomColor(): Int {
            val random = Random()
            return Color.argb(255, random.nextInt(256), random.nextInt(256), random.nextInt(256))
        }
    }
}
