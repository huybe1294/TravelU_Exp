package com.huypt.travelu_exp

object Constant {
    fun makeURL(oriLat: String, oriLon: String, desLat: String, desLon: String): String {
        val url = StringBuilder()

        with(url) {
            append("https://maps.googleapis.com/maps/api/directions/json")
            append("?origin=")
            append(oriLat)
            append(",")
            append(oriLon)
            append("&destination=")
            append(desLat)
            append(",")
            append(desLon)
            append("&key=")
            append("AIzaSyDi5fWVKd7oGh4Et8pKlHz12Q6g6p3wuyk")
//            append("&alternatives=true")
        }
        return url.toString()
    }
}