package com.huypt.travelu_exp

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import com.bumptech.glide.Glide
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker
import kotlinx.android.synthetic.main.infor_window_image.view.*

class InforWindowAdapter(private var context: Context) : GoogleMap.InfoWindowAdapter {

    override fun getInfoContents(p0: Marker?): View? {
        return null
    }

    @SuppressLint("InflateParams")
    override fun getInfoWindow(p0: Marker?): View {
        val view = LayoutInflater.from(context).inflate(R.layout.infor_window_image, null)
        Glide.with(context).load(R.drawable.index).into(view.imgPhoto)
        return view
    }
}