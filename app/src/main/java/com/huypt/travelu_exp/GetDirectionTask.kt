package com.huypt.travelu_exp

import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import com.huypt.travelu_exp.model.Directions
import com.huypt.travelu_exp.model.StepsItem
import java.io.*
import java.net.HttpURLConnection
import java.net.URL

class GetDirectionTask {

    fun getDirection(urlRequest: String): MutableList<String> {
        val data = mutableListOf<String>()
        var inputStream: InputStream? = null
        var conn: HttpURLConnection? = null
        try {
            val url = URL(urlRequest)

            conn = url.openConnection() as HttpURLConnection
            inputStream = conn.inputStream
            val reader = InputStreamReader(inputStream)

            val results = Gson().fromJson(reader, Directions::class.java)
            val routes = results.routes
            val leg = routes?.get(0)?.legs
            val steps = leg?.get(0)?.steps
//            if (steps != null)
//                data = steps

            if (steps != null)
                for (item in steps) {
                    val points = item.polyline.points
                    data.add(points)
                }

        } catch (e: IOException) {
            e.printStackTrace()
        } finally {
            inputStream?.close()
            conn?.disconnect()
        }
        return data
    }
}